from flask import Flask, request
import socket
import datetime

app = Flask(__name__)

@app.route('/', defaults={'path': ''})
@app.route('/<path:path>')
def catch_all(path):
    now = datetime.datetime.now()
    timenow = now.strftime("%Y-%m-%d %H:%M:%S")
    docker_short_id = socket.gethostname()
    container_msg = " container ID:" + docker_short_id

    if path != '':
        msg_resp = timenow + " Request path: /" + path + " and reply from " + container_msg + "\n"
        return msg_resp
    msg_resp = timenow + " Reply from " + container_msg + "\n"
    return msg_resp


if __name__ == '__main__':
    app.run(host='0.0.0.0')
